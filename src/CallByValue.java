import java.text.SimpleDateFormat;
import java.util.Date;

public class CallByValue {

	int data = 50;

	void add(CallByValue obj) {
		obj.data = data + 400;

	}

	void sum(int data) {
		data = data + 400;
	}

	public static void main(String[] args) {

		CallByValue obj = new CallByValue();

		System.out.println("before changes : " + obj.data);

		obj.sum(50);

		System.out.println("after change : " + obj.data);

		System.out.println("************************************");

		System.out.println("before changes : " + obj.data);

		obj.add(obj);

		System.out.println("after change : " + obj.data);
		
		
		
		
		
		
		

	}

}
