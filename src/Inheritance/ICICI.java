package Inheritance;

public class ICICI extends Bank {

	int a=34;
	
	public ICICI() {
		
		super(9);
		System.out.println("in icici");
		
	}
	public void print() {
		System.out.println("in ring");
	}
	
	public void display() {
		show();
		System.out.println("in bank");
	}

	public int returnRateOfInterest() {
		System.out.println("icici a:- "+a);
		
		System.out.println("super a:- "+super.a);
		int i = super.returnRateOfInterest();
		System.out.println(i);
		
		return 23;
	}
}
