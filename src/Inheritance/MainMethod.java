package Inheritance;

public class MainMethod {

	public static void main(String[] args) {
//		Child obj  = new Child();
//		obj.display();
//		obj.show();

		ICICI obj;
		obj = new ICICI();
//		obj.display();
//		obj.returnRateOfInterest();
		String str = "icici";

		Bank obj1 = null;

		if (str.equalsIgnoreCase("icici")) {
			obj1 = new ICICI(); // run time polym, dynamic method dispatch, upcasting,dynamic binding, late
								// binding
		} else if (str.equalsIgnoreCase("IDFC")) {
			obj1 = new IDFC(23);
		}

		obj1.returnRateOfInterest();
		
		
	
		
	}

}
