package Inheritance;

public abstract class Shape {
	String colour;

	abstract double area();

	public abstract String toString();

	public Shape(String colour) {
		System.out.println("in shape");
		this.colour = colour;
	}

	public String getColour() {
		return this.colour;
	}

}
