package Inheritance;

public class Circle extends Shape {

	double radius;

	public Circle(String colour,double radius) {
		super(colour);
		this.radius = radius;
		System.out.println("in circle");

	}

	@Override
	double area() {
		double a = Math.PI * Math.pow(radius, 2);
		return a;
	}

	@Override
	public String toString() {

		return "Circle colour is :" + super.getColour() + " and Area is : " + area();

	}

}
