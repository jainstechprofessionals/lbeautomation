package ExceptionHandling;

public class ExceptionClassExample {

	public static void main(String[] args) {

		try {

			int a = 20;
			int b = 0;
			System.out.println(a / b);

			String str = null;

			System.out.println(str.length());

		}

		catch (ArithmeticException e) {
			System.out.println("Please enter a non zero value");
			e.printStackTrace();

		} catch (NullPointerException e) {
			System.out.println(" please enter the name");
		} catch (Exception e) {
			System.out.println("in catch block ");
		}
		finally {
			System.out.println("in finally");
		}

	}

}
