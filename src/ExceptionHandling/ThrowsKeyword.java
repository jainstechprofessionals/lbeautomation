package ExceptionHandling;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ThrowsKeyword {
	
	
	
	public void show() throws InterruptedException, FileNotFoundException {
		ThrowKeyword.validate(13);
		Thread.sleep(2000);
		
		File f = new File("");
		FileInputStream fis = new FileInputStream(f);
		
		
	}

	public static void main(String[] args)  {
		
		ThrowsKeyword obj =new ThrowsKeyword();
	
		

	}

}
