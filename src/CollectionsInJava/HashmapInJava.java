package CollectionsInJava;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class HashmapInJava {

	public static void main(String[] args) {

		HashMap<Integer, String> hs = new HashMap<Integer, String>();

		hs.put(104, "Shivali");
		hs.put(102, "julee");
		hs.put(101, "abhishek");

		hs.put(103, "Rounak");

		hs.put(102, "nikhil");
		hs.put(null, null);
//	hs.put(null, "sdfa");
		hs.put(25, null);

		hs.replace(104, "asdfsa");
//		Set<Integer> s = hs.keySet();
//		Iterator<Integer> it = s.iterator();
//		
//		while(it.hasNext()) {
//			System.out.println(it.next());
//		}
//		
		
		System.out.println(hs.get(0));

		for (Map.Entry m : hs.entrySet()) {

			System.out.println(m.getKey() + "    " + m.getValue());
		}

	}

}
