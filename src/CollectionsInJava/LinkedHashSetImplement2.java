package CollectionsInJava;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class LinkedHashSetImplement2 {

	public static void main(String[] args) {

		LinkedHashSet<String> hs = new LinkedHashSet<String>();		
		
		hs.add("rounak");
		hs.add("Rashika");
		hs.add("Shivali");
		hs.add("Nikhil");	
		hs.add("Nikhil");
		hs.add(null);
		
		
		
		Iterator itr = hs.iterator();
		
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}
		

	}

}
