
public class ConstructorInJava {

	public ConstructorInJava() {
		System.out.println("in default constructor");
	}
	
	public ConstructorInJava(int n) {
		System.out.println(n);
	}
	
	
	
	public static void main(String[] args) {
		
		ConstructorInJava obj=	new ConstructorInJava();
	
	
		ConstructorInJava obj1=	new ConstructorInJava(10);

	}

}
