
public class JaggedArray {

	
	
	public static void main(String[] args) {
		
		int temp=1;
		
		int arr [][]= new int [3][];  // jagged array
		
		arr[0]= new int[4];  // creating columns for each row
		arr[1]=new int [2];
		arr[2]=new int [3];
		
		for (int i = 0; i < arr.length; i++) {

			for (int j = 0; j < arr[i].length; j++) {
				arr[i][j] = temp;
				temp++;
			}
		}
		
		
		for (int i = 0; i < arr.length; i++) {

			for (int j = 0; j < arr[i].length; j++) {
//				System.out.println("Row :" + i+ "and column:" +j+ "    " +arr[i][j]);
				System.out.print(arr[i][j] + "\t" );
			
			}
			System.out.println();
		}
	}

}
