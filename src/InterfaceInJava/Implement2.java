package InterfaceInJava;

import java.lang.String;

public class Implement2 extends PackageClass implements InterfaceA {

	InterfaceA obj;
	String str = "rounak";

	@Override
	public void print() {

		System.out.println("Implement2 print");
	}

	@Override
	public InterfaceA show() {
		obj = new Implement2();		
		return obj;
	}

}
