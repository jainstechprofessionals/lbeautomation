
public class StaticInJava {

	static String school = "LNCT Bhopal";
	String name = null;
	static int i = 10;

	
	
	static {
		System.out.println("in static block");
		System.exit(0);
	}
	
	public static void display() {
		System.out.println("in display");
		

	}

	public void set(String n) {
		
		
		name = n;

	}

	public void print() {
		System.out.println(school);
		System.out.println(name);

	}

	public void show() {
		System.out.println(i);
		i++;
	}

	public static void main(String[] args) {
		StaticInJava obj = new StaticInJava();
		obj.set("rounak");
		obj.print();
		obj.show();
		System.out.println("*********************");
		StaticInJava obj1 = new StaticInJava();

		obj1.set("shivali");
		obj1.print();
		obj1.show();
		StaticInJava.display();

		System.out.println(Math.PI);

	}

}
