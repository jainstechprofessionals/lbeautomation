package WeekEndARU;

public class ConstructorInJava {

	int bonus;
	
	
	public ConstructorInJava() {
		bonus=100;
		System.out.println("in constructor");
		
	}
	
	public ConstructorInJava(int b) {
		bonus = b;
		System.out.println("in parametrized constructor");
		
	}
	
	

	public void inital(int b) {
		bonus = b;
	}

	public void show() {
		System.out.println(bonus);
	}

	public void bonusCal(int sal, String name) {

		System.out.println("Employee " +name+ " will get salary as " + (sal + bonus));
	}

	public static void main(String[] args) {

	
		
		ConstructorInJava o = new ConstructorInJava();
		ConstructorInJava o1 = new ConstructorInJava(900);
		o.bonusCal(1000,"rounak");
		o.bonusCal(2300,"prabhat");
		
		
		
			o1.bonusCal(1000,"rounak");
			o1.bonusCal(2300,"prabhat");
		
	}

}
