package WeekEndARU;

public class LoopsInJava {

	public static void main(String[] args) {

		// 1. Entry control (while , for )

		// 2. Exit control (do while)

//		int a =2;
//		
//		while(a<=20) { // 4<=20
//			System.out.println(a);
//			a=a+2;
//		}

//		for (int i = 2; i <= 20; i = i + 2) {
//			
//			System.out.println(i);
//
//		}
		
		int a=2;
		do {
			System.out.println(a);
			a=a+2;
			
		}while(a<=20);

	}
}