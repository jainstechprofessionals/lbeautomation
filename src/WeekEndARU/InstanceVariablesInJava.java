package WeekEndARU;

public class InstanceVariablesInJava {

	int a = 10; // instance variable or field or global variable

	public void show() {
		int k = 10; // local variable
		System.out.println(a);
		long a = 12;
		System.out.println(a);
		System.out.println("in show");
	}

	public void display() {
		System.out.println(a);
	}

	public static void main(String[] args) {
		InstanceVariablesInJava obj1 = new InstanceVariablesInJava();
		obj1.show();
		obj1.display();
	}

}
