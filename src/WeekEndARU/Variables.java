package WeekEndARU;

public class Variables {

	public static void main(String[] args) {

		
		
		
		int a;

//		a = 10;
//		System.out.println(a);
//		a=20;
//		System.out.println(a );

		boolean b = true; // 1 bit

		char c = 'a'; // 2 byte

		byte by = 12; // 1 byte // -128 to 127

		short s = 34; // 2 byte // -32768 to 32767

		int i = 44; // 4 byte // -2^31 to 2^31-1

		long l = 332; // 8 byte // -2^63 to 2^63-1

		float f = 34.3f; // 4 byte

		double d = 34.4d;

//		********************** Non Primitive DataTypes  **************

		String str = "Rounak";

//		******************* Type Conversion ***************

//		1. Implicitly 
		// 2. Explicit
		
		i=s;
		
		s=(short) i;
		
		i=(int) f;
		
		System.out.println(i);
		System.out.println(f);
		
		
		
		

	}

}
