package WeekEndARU;

public class ThisInJava {

	
	int i =10;
	
	ThisInJava(){
		System.out.println("in non parametrized");
	}
	ThisInJava(int i,int k){
		
	}
	
	ThisInJava(int i){
		
		this(12,12);
		System.out.println("in para");
	}
	
	public void show(int i) {
//		this.i=i;
		System.out.println(i);
		System.out.println(this.i);
	}
	
	public void print() {
		this.show(12);
		System.out.println("in print");
	}
	

	
	public static void main(String[] args) {
		
		
	}

}
