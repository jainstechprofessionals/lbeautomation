package WeekEndARU;

public class StaticInJava {
	
	

	 int rollNo;
	String name;
	static String college = "LNCT Bhopal";

	StaticInJava(int a, String n) {
		rollNo = a;
		name = n;
	}

	public static void display() {
	
		System.out.println("in display");
	}
	
	public void print() {
		
		
		System.out.println(rollNo);
		System.out.println(name);
		System.out.println(college);
	}
	
	public static void main(String[] args) {
		StaticInJava.display();
		
		StaticInJava s = new StaticInJava(101,"rounak");
		
		StaticInJava s1= new StaticInJava(102, "mohan");
		
		
		s.print();
		
		
		StaticInJava.college = "LNCT & Rece Bhopal";
		System.out.println("*********************");
		s.print();
		
		System.out.println("*********************");
		
		s1.print();

	}

}
