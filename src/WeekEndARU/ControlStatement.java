package WeekEndARU;

public class ControlStatement {

	public static void main(String[] args) {
		
		
//		int a =1;
//		
//		// if -else
//		// rem == 0
//		
//		// if 
//		// if -else
//		// if - else if 
//		// nested if
//		
//		if(a%2==0) {
//			System.out.println("even number");
//		}else {
//			System.out.println("odd number");
//		}
		
		
		int a =10;
		int b =45;
		int c =32;
		
		// if - else
		// a > b && a >c
		// b > c
		
		// Logical Operators ( && , ||)
		
		if(a>b && a>c) {
			System.out.println("a is largest");
		}else if(b>c) {
			System.out.println("b is largest");
		}else {
			System.out.println("c is largest");
		}

		
		
	}

}
