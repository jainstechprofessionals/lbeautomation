package WeekEndARU;

public class VariablesInJava {

	public static void main(String[] args) {

		// ******** Data type ******

//		1. Primitive 


		boolean b = false; // 1 bit
		char c = 'A'; // 2 byte '\u0000'
		byte y = 16; // 1 byte   (-128 to 127)
		short s = 23; // 2 byte (-32767 to 32768)
		int i = 23; // 4 byte
		long l = 23; // 8 byte
		
		float f = 23.34f;  // 4 byte
		double d = 34.4d;  // 8 byte
		

		System.out.println(c);
		
		
		// type conversion 
		// 1. implict 
		// 2. Explicit 
		
		i=s;
		s= (short) i;
		
		
		
		// //		2. Non Primitive
		
		
		String str = "rounak";
		

	}

}
