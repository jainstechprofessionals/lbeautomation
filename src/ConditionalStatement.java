
public class ConditionalStatement {

	public static void main(String[] args) {

		int a = 2;
		int b = 4;
//
		if (a % 2 == 0) {
			System.out.println("even number");
		} else {
			System.out.println("odd number");
		}

		// logical operator
//		
//		if(b==3 && a==2) {
//			System.out.println("true");
//		}else {
//			System.out.println("False");
//		}

		
		if(b==3 || a==2) {
			System.out.println("true");
		}else {
			System.out.println("False");
		}
		
		
		
	}

}
